<?php 
session_start();
include '../config.php';
if ( isset($_COOKIE['id']) && isset($_COOKIE['key'])) {
	
    $id = $_COOKIE['id'];
    $key = $_COOKIE['key'];
	
	$result = mysqli_query($connect, "select users.*, pegawai.jabatan from users 
								LEFT JOIN pegawai ON users.id_pegawai = pegawai.id WHERE users.id = '$id'");

    $row = mysqli_fetch_assoc($result);
    if($key == $row['username']){
	  $_SESSION["username"] = $row['username'];
	  $_SESSION["jabatan"] = $row['jabatan'];
      $_SESSION['login'] = true;
    }
}

if ( isset($_SESSION['login']) ) {
	header("Location: ../dashboard/index.php");
  exit;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="card">
		<h3>Login</h3>
        <?php 
        if(isset($_GET['status'])){
           echo '<em> '.$_GET['status'].' </em>';
        }
        ?>
		<form action="login-proses.php" method="POST">
			<label>Username</label>
			<input type="text" name="username" class="form-control" placeholder="username">
 
			<label>Password</label>
			<input type="password" name="password" class="form-control" placeholder="password">

			<input type="checkbox" name="remember-me"> Remember me
       		
 
			<input type="submit" class="tombol" value="LOGIN" name="login">
		</form>
		
	</div>
 
 
</body>
</html>
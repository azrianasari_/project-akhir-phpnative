<?php

include("../config.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: index.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM pemilik WHERE id=$id";
$query = mysqli_query($connect, $sql);
$row = mysqli_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}
?>

<?php 
$page = "Pemilik";
include "../dashboard/header.php"; ?>
<div class="isi">

<body>
    <h3>Detail Data</h3>
    <form action="update-proses.php" method="POST">
        <fieldset>
            <table>
                <input type="hidden" name="id" value=<?= $row['id'] ?> />
                <tr>
                    <td><label for="kode">Kode: <?= $row['kode']?>  </label></td>
                </tr>
                <tr>
                    <td><label for="nama">Nama: <?= $row['nama']?>  </label></td>
                </tr>
                <tr>
                    <td><label for="alamat">Alamat: <?=$row['alamat']?> </label></td>
                </tr>
                <tr>
                    <td><label for="kecamatan">Kecamatan: <?=$row['kecamatan']?> </label></td>
                </tr> 
                <tr>
                    <td><label for="kelurahan">Kelurahan: <?=$row['kelurahan']?> </label></td>
                </tr> <tr>
                    <td><label for="kab_kota">Kab/Kota: <?=$row['kab_kota']?> </label></td>
                </tr>
                <tr>
                    <td><label for="kode_pos">Kode Pos: <?= $row['kode_pos']?> </label></td>
                </tr>
                <tr>
                    <td><label for="email">Email: <?= $row['email']?> </label></td>
                </tr>
                <tr>
                    <td><label for="telp">Telepon:  <?= $row['telp']?> </label></td>
                </tr>
                <tr>
                    <td><a href="index.php">Kembali</td>
                </tr>
            </table>

        </fieldset>
    </form>
<?php include "../dashboard/footer.php"; ?>

<?php 
$page = "Pemilik";
include "../dashboard/header.php"; 
include "../config.php";

$result = mysqli_query($connect,"select*from pemilik");
?>
<div class="isi">
<h3>List Data</h3>
<?php 

if(isset($_GET['status'])){
   echo '<h3> '.$_GET['status'].' </h3>';
}

?>

<a href="tambah-form.php">[+] Tambah Baru</a>

<table border="1" cellpadding="8" cellspacing="0">
        <thead>
            <th>NO</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Telepon</th>
            <th></th>
        </thead>
        <tbody>

        <?php 
            $index = 1;      
        ?>
      
        <?php while ($row = mysqli_fetch_array($result)) { ?>
            <tr>
                <td><?= $index++ ?></td>   
                <td><?= $row['nama'] ?></td>
                <td><?= $row['alamat'] ?></td>
                <td><?= $row['telp'] ?></td>
                <td>
                    <a href='edit-form.php?id="<?= $row['id'] ?>"'>Edit</a> | 
                    <a href='hapus-proses.php?id=<?= $row['id'] ?>' onClick="return confirm('yakin hapus ?')">Hapus</a> |
                    <a href='detail-form.php?id="<?= $row['id'] ?>"'>Detail</a> 
                </td>
            </tr>
        <?php } ?>  
      
      </tbody>
</table>
</div>
<?php include "../dashboard/footer.php" ?>

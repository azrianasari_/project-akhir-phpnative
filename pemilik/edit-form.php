<?php

include("../config.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: index.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM pemilik WHERE id=$id";
$query = mysqli_query($connect, $sql);
$row = mysqli_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}

?>
<?php 
$page = "Pemilik";
include "../dashboard/header.php"; ?>
<div class="isi">

<body>
    <h3>Edit Data</h3>
  
    <form action="update-proses.php" method="POST">

        <fieldset>

            <table>
                <input type="hidden" name="id" value=<?= $row['id'] ?> />
                <tr>
                    <td><label for="kode">Kode: </label></td>
                    <td><input type="text" name="kode" id="kode" placeholder="kode" value = <?= $row['kode']?> required ></td>
                </tr>
             
                <tr>
                    <td><label for="nama">Nama: </label></td>
                    <td><input type="text" name="nama" id="nama" placeholder="nama" value = <?= $row['nama']?> required ></td>
                </tr>
                <tr>
                    <td><label for="alamat">Alamat: </label></td>
                    <td><textarea name="alamat" id="alamat" required><?= $row['alamat']?></textarea></td>
                </tr>
                <tr>
                    <td><label for="kecamatan">Kecamatan: </label></td>
                    <td><input type="text" name="kecamatan" id="kecamatan" placeholder="kecamatan" value = <?= $row['kecamatan']?> required ></td>
                </tr>
                <tr>
                    <td><label for="kelurahan">Kelurahan: </label></td>
                    <td><input type="text" name="kelurahan" id="kelurahan" placeholder="kelurahan" value = <?= $row['kelurahan']?> required  ></td>
                </tr>
                <tr>
                    <td><label for="kab">Kab/Kota: </label></td>
                    <td><input type="text" name="kab" id="kab" placeholder="kab" value = <?= $row['kab_kota']?> required ></td>
                </tr>
                <tr>
                    <td><label for="kode_pos">Kode Pos: </label></td>
                    <td><input type="text" name="kode_pos" id="kode_pos" placeholder="kode pos" value = <?= $row['kode_pos']?> required  /></td>
                </tr>
                <tr>
                    <td><label for="email">Email: </label></td>
                    <td><input type="text" name="email" id="email" placeholder="email" value = <?= $row['email']?> required ></td>
                </tr>
                <tr>
                    <td><label for="telp">Telepon: </label></td>
                    <td><input type="text" name="telp" id="telp" placeholder="0823xxx" value = <?= $row['telp']?> required ></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="update" name="update" /></td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>

<?php include "../dashboard/footer.php" ?>
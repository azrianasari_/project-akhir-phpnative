<?php 
$page = "Pemilik";
include "../dashboard/header.php";

?>
<div class="isi">
    <h3>Tambah Data</h3>
 
    <form action="tambah-proses.php" method="POST">

        <fieldset>

            <table>
                <tr>
                    <td><label for="kode">Kode: </label></td>
                    <td><input type="text" name="kode" id="kode" placeholder="P001" required/>
                    </td>
                </tr>
             
                <tr>
                    <td><label for="nama">Nama: </label></td>
                    <td><input type="text" name="nama" id="nama" placeholder="nama" required/>
                </tr>
                <tr>
                    <td><label for="alamat">Alamat: </label></td>
                    <td><textarea name="alamat" id="alamat" required></textarea>
                </td>
                   
                </tr>
                <tr>
                    <td><label for="kecamatan">Kecamatan: </label></td>
                    <td><input type="text" name="kecamatan" id="kecamatan" placeholder="kecamatan" required/></td>
                </tr>
                <tr>
                    <td><label for="kelurahan">Kelurahan: </label></td>
                    <td><input type="text" name="kelurahan" id="kelurahan" placeholder="kelurahan" required/></td>
                </tr>
                <tr>
                    <td><label for="kab">Kab/Kota: </label></td>
                    <td><input type="text" name="kab" id="kab" placeholder="kab" required/></td>
                </tr>
                <tr>
                    <td><label for="kode_pos">Kode Pos: </label></td>
                    <td><input type="text" name="kode_pos" id="kode_pos" placeholder="Kode pos" required/></td>
                </tr>
                <tr>
                    <td><label for="email">Email: </label></td>
                    <td><input type="text" name="email" id="email" placeholder="email"  required/></td>
                </tr>
                <tr>
                    <td><label for="telp">Telepon: </label></td>
                    <td><input type="text" name="telp" id="telp" placeholder="0823xxx" required/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="tambah" name="tambah" /></td>
                </tr>
            </table>

        </fieldset>

    </form>
</div>

<?php include "../dashboard/footer.php" ?>
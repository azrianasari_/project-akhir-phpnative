-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 03, 2020 at 11:48 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rental`
--

-- --------------------------------------------------------

--
-- Table structure for table `mobil`
--

CREATE TABLE `mobil` (
  `id` int(11) NOT NULL,
  `kode` char(4) NOT NULL,
  `tahun` int(4) NOT NULL,
  `warna` varchar(15) NOT NULL,
  `no_plat` varchar(12) NOT NULL,
  `no_mesin` char(7) NOT NULL,
  `no_rangka` char(16) NOT NULL,
  `status_mobil` int(1) NOT NULL,
  `merk` varchar(12) NOT NULL,
  `tipe` varchar(30) NOT NULL,
  `foto` varchar(30) NOT NULL,
  `id_pemilik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobil`
--

INSERT INTO `mobil` (`id`, `kode`, `tahun`, `warna`, `no_plat`, `no_mesin`, `no_rangka`, `status_mobil`, `merk`, `tipe`, `foto`, `id_pemilik`) VALUES
(1, 'M001', 2012, 'Hitam', 'KT 2012', '0041', '0043', 0, 'Honda', 'Sedan', 'mobil1.jpg', 1),
(4, 'M002', 2010, 'Putih', 'KT 2012', '0042', '0044', 1, 'Honda', 'Sedan', 'mobil1.jpg', 125);

-- --------------------------------------------------------

--
-- Table structure for table `pemilik`
--

CREATE TABLE `pemilik` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` varchar(30) DEFAULT NULL,
  `kelurahan` varchar(30) DEFAULT NULL,
  `kecamatan` varchar(30) DEFAULT NULL,
  `kab_kota` varchar(30) DEFAULT NULL,
  `kode` char(4) NOT NULL,
  `kode_pos` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `telp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemilik`
--

INSERT INTO `pemilik` (`id`, `nama`, `alamat`, `kelurahan`, `kecamatan`, `kab_kota`, `kode`, `kode_pos`, `email`, `telp`) VALUES
(1, 'Azriana', 'Juanda', 'Sidodadi', 'Samarinda Ulu', 'Samarinda', 'P001', '1234', NULL, ''),
(124, 'Ndrik', 'Samarinda', 'samarinda', 'samarinda', 'samarinda', 'P002', '1235', NULL, ''),
(125, 'Rama', 'Jalan Pahlawan No.2 A', 'Dadi Mulya', 'Samarinda Ulu', 'Samarinda', 'P003', '75123', 'rama@mail.com', '082350014322'),
(128, 'Sari', 'PM Noor', 'Kelurahan', 'Kecamatan', 'Kabupaten', '', '123123', 'azriana@smkti.id', '08222222222');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `id_pegawai`) VALUES
(1, 'admin', '8fc828b696ba1cd92eab8d0a6ffb17d6', 1),
(4, 'dani', '8fc828b696ba1cd92eab8d0a6ffb17d6', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mobil`
--
ALTER TABLE `mobil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pemilik` (`id_pemilik`);

--
-- Indexes for table `pemilik`
--
ALTER TABLE `pemilik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mobil`
--
ALTER TABLE `mobil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pemilik`
--
ALTER TABLE `pemilik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

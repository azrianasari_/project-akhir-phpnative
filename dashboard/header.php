<?php
session_start();

function jabatan($data) {
    if ($data == 0){
        echo "Admin";
    }else {
        echo "Kasir";
    }
}
if( !isset($_SESSION['login'])) {
    header("Location:../login/index.php");
    exit;
}?>

<!DOCTYPE html>
<html>
<head>
    <title><?php $page ?></title>
    <link rel="stylesheet" type="text/css" href="../style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <div class="container">
       <header>
            <div class="logo">
              <img src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png" alt="" width="5%"> eCar Rent
           </div>
       </header>
       <nav>
            <?php if($_SESSION['jabatan'] == "0" ) { ?>
            
                 <ul>
                    <li>  <a href="../dashboard/index.php" <?php if($page == "Dashboard") {echo "class= 'active'";}?>>Dashboard</a> </li>
                     <li> <a href="../pemilik/index.php" <?php if($page == "Pemilik") {echo "class= 'active'";}?>>Pemilik</a> </li>
                     <li> <a href="../mobil/index.php"  <?php if($page == "Mobil") {echo "class= 'active'";}?>>Mobil</a> </li>
                     <li> <a href="../users/index.php"  <?php if($page == "Users") {echo "class= 'active'";}?>>Users</a> </li>
                     <li> <a href="../login/logout.php" >Logout</a> </li>
                 </ul>
            
            
             <?php } else { ?>
                 <ul>
                     <li> <a href="../dashboard/index.php" <?php if($page == "Dashboard") {echo "class= 'active'";}?>>Dashboard</a> </li>
                     <li> <a href="../login/logout.php" >Logout</a> </li>
                 </ul>

             <?php } ?>

        </nav>
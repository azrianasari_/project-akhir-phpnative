<?php
include "../config.php";

$pegawai = mysqli_query($connect,"select*from pegawai");

$page = "Users";
include "../dashboard/header.php";
?>

<div class="isi">
    <h3>Tambah Data</h3>
 
    <form action="tambah-proses.php" method="POST">

        <fieldset>

            <table>
                <tr>
                    <td><label for="id_pegawai">Pegawai: </label></td>
                    <td>
                        <select name="id_pegawai" id="id_pegawai">
                        <?php while ($pegawais = mysqli_fetch_array($pegawai)) { ?>
                            <option value="<?= $pegawais['id']?>"><?= $pegawais['kode']. ' - ' . $pegawais['nama'] ?></option>
                        <?php } ?>  
                            
                        
                        </select>
                       
                    </td>
                </tr>
                <tr>
                    <td><label for="username">Username: </label></td>
                    <td><input type="text" name="username" id="username" placeholder="nama" /></td>
                </tr>
             
                <tr>
                    <td><label for="password">Password: </label></td>
                    <td><input type="text" name="password" id="password" placeholder="password" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="tambah" name="tambah" /></td>
                </tr>
            </table>

        </fieldset>

    </form>

</div>


<?php include "../dashboard/footer.php" ?>
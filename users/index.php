<?php
include "../config.php";

$result = mysqli_query($connect, "select users.*, pegawai.nama, pegawai.jabatan from users 
                                    LEFT JOIN pegawai ON users.id_pegawai = pegawai.id ");

$page = "Users";
include "../dashboard/header.php";
?>

<div class="isi">

<h3>List Data</h3>
<?php 

if(isset($_GET['status'])){
   echo '<h3> '.$_GET['status'].' </h3>';
}

?>

<a href="tambah-form.php">[+] Tambah Baru</a>

<table border="1" cellpadding="8" cellspacing="0">
        <thead>
            <th>No</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Username</th>
            <th></th>
        </thead>
        <tbody>

        <?php 
            $index = 1;      
        ?>
      
        <?php while ($row = mysqli_fetch_array($result)) { ?>
            <tr>
                <td><?= $index++ ?></td>   
                <td><?= $row['nama'] ?></td>
                <td><?php  if($row['jabatan']  == 0) { echo "admin"; } else { echo "kasir";}?>
                </td>
                <td><?= $row['username'] ?></td>
                <td>
                    <a href='edit-form.php?id="<?= $row['id'] ?>"'>Edit</a> | 
                    <a href='hapus-proses.php?id=<?= $row['id'] ?>' onClick="return confirm('yakin hapus ?')">Hapus</a>
                </td>
            </tr>
        <?php } ?>  
      
      </tbody>
</table>
    
</div>


<?php include "../dashboard/footer.php" ?>

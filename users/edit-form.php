<?php

include("../config.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: index.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM users WHERE id=$id";
$query = mysqli_query($connect, $sql);
$row = mysqli_fetch_assoc($query);

$pegawai = mysqli_query($connect,"select*from pegawai");


// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}

$page = "Users";
include "../dashboard/header.php";
?>

<div class="isi">
    <h3>Edit Data</h3>
  
    <form action="update-proses.php" method="POST">

        <fieldset>

        <table>
                <input type="hidden" name="id" value=<?= $row['id'] ?> />
                
                <tr>
                    <td><label for="id_pegawai">Pegawai: </label></td>
                    <td>
                        <select name="id_pegawai" id="id_pegawai">
                        <?php while ($pegawais = mysqli_fetch_array($pegawai)) { ?>
                            <option value="<?= $pegawais['id']?>" <?php if($row['id_pegawai'] == $pegawais['id']) { echo "selected"; } ?> ><?= $pegawais['kode']. ' - ' . $pegawais['nama'] ?></option>
                        <?php } ?>  
                            
                        
                        </select>
                       
                    </td>
                </tr>
                <tr>
                    <td><label for="username">Username: </label></td>
                    <td><input type="text" name="username" id="username" placeholder="nama" value="<?= $row['username']?>" /></td>
                </tr>
             
                <tr>
                    <td><label for="password">Password: </label></td>
                    <td><input type="text" name="password" id="password" placeholder="password" /><br>
                            <span style="font-size:10px;color:red;"> *kosongkan password jika tidak dirubah </span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="update" name="update" /></td>
                </tr>
            </table>

        </fieldset>
    </form>
</div>


<?php include "../dashboard/footer.php" ?>
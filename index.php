<?php
include "config.php";

$result = mysqli_query($connect,"select mobil.*,
                                pemilik.nama from mobil LEFT JOIN pemilik  ON mobil.id_pemilik = pemilik.id;");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Beranda</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <div class="container">
       <header>
           <div class="logo">
              <img src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png" alt="" width="5%"> eCar Rent
           </div>
       </header>
       <nav>
       <ul>
                <li><a href="index.php" class="active">Beranda</a></li>
                <li><a href="kontak.php">Kontak</a></li>
                <li><a href="login/index.php">Login</a></li>
            </ul>
        </nav>
       <article>
       <?php while ($row = mysqli_fetch_array($result)) { ?>
           <div class="konten">
                <a href='detail.php?id=<?= $row['id'] ?>'>
                    <img src="mobil/upload/<?= $row['foto'] ?>" ></a>
                    <div class="judul">
                    <a href='detail.php?id=<?= $row['id'] ?>'><?= $row['merk'] ?></a>
                    </div>
                    <p><?= $row['tipe'] . ' - '?><?php if($row['status_mobil'] == "0") { echo 'Tersedia';  } else {  echo "Tidak Tersedia"; }?></p>
           </div>
       <?php } ?>
      </article>
  
       <footer>
            Copyright 2020
       </footer>
    </div>
</body>
</html>
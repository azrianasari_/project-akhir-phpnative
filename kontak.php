<!DOCTYPE html>
<html>
<head>
    <title>Beranda</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <div class="container">
       <header>
           <div class="logo">
              <img src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png" alt="" width="5%"> eCar Rent
           </div>
       </header>
       <nav>
            <ul>
                <li><a href="index.php">Beranda</a></li>
                <li><a href="kontak.php " class="active">Kontak</a></li>
                <li><a href="login/index.php">Login</a></li>
            </ul>
        </nav>
       <article>
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.674732300062!2d117.14561301409009!3d-0.48609539964535414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2df67f3f059f0b07%3A0x2cb16130a424f1b6!2sJl.%20Pahlawan%2C%20Kec.%20Samarinda%20Ulu%2C%20Kota%20Samarinda%2C%20Kalimantan%20Timur%2075242!5e0!3m2!1sen!2sid!4v1604161108685!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
           <table>
                <tr>
                    <td><label>Alamat</label></td>
                    <td><label>:</label></td>
                    <td><label>Jl. Pahlawan No.2 A, Dadi Mulya, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75123</label></td>
                </tr>
                <tr>
                    <td><label>Jam buka</label></td>
                    <td><label>:</label></td>
                    <td><label>Senin s/d Jumat pukul 10.00 - 18.00 wita</label></td>
            
                </tr>
                <tr>
                    <td><label>Telepon</label></td>
                    <td><label>:</label></td>
                    <td><label>(0541) 741866</label></td>
                </tr>
            </table>
        
      </article>
  
       <footer>
            Copyright 2020
       </footer>
    </div>
</body>
</html>
<?php
include "config.php";
// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: index.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$query = mysqli_query($connect,"select mobil.*,pemilik.nama from mobil LEFT JOIN pemilik  ON mobil.id_pemilik = pemilik.id WHERE mobil.id=$id");
$row = mysqli_fetch_assoc($query);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Beranda</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <div class="container">
       <header>
           <div class="logo">
              <img src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png" alt="" width="5%"> eCar Rent
           </div>
       </header>
       <nav>
            <ul>
                <li><a href="index.php">Beranda</a></li>
                <li><a href="kontak.php">Kontak</a></li>
                <li><a href="login/index.php">Login</a></li>
            </ul>
        </nav>
       <article>
            <div class="gambar-detail">
               <img src="mobil/upload/<?= $row['foto']?>" alt="" width="60%"> 
            </div>
           <table>
                <tr>
                    <td><label for="kode">Kode</label></td>
                    <td><label for="kode">:</label></td>
                    <td><label for="kode"><?= $row['kode']?>  </label></td>
                </tr>
                <tr>
                    <td><label for="tahun">Tahun</label></td>
                    <td><label for="tahun">:</label></td>
                    <td><label for="tahun"><?= $row['tahun']?></label></td>
            
                </tr>
                <tr>
                    <td><label for="warna">Warna</label></td>
                    <td><label for="warna">:</label></td>
                    <td><label for="warna"><?=$row['warna']?> </label></td>
                </tr>
                <tr>
                    <td><label for="no_plat">No plat</label></td>
                    <td><label for="no_plat">:</label></td>
                    <td><label for="no_plat"><?=$row['no_plat']?> </label></td>
                </tr> 
                <tr>
                    <td><label for="no_mesin">No mesin</label></td>
                    <td><label for="no_mesin">:</label></td>
                    <td><label for="no_mesin"><?=$row['no_mesin']?> </label></td>
                </tr> <tr>
                    <td><label for="no_rangka">No rangka</label></td>
                    <td><label for="no_rangka">:</label></td>
                    <td><label for="no_rangka"><?=$row['no_rangka']?> </label></td>
                </tr>
                <tr>
                    <td><label for="status_mobil">Status mobil</label></td>
                    <td><label for="status_mobil">:</label></td>
                    <td><label for="status_mobil"><?php if($row['status_mobil'] == "0") { echo 'Tersedia'; } else { echo "Tidak Tersedia";  }?> </label></td>
                </tr>
                <tr>
                    <td><label for="merk">Merk</label></td>
                    <td><label for="merk">:</label></td>
                    <td><label for="merk"><?= $row['merk']?> </label></td>
                </tr>
                <tr>
                    <td><label for="tipe">Tipe</label></td>
                    <td><label for="tipe">:</label></td>
                    <td><label for="tipe"><?= $row['tipe']?> </label></td>
                </tr>
            </table>
        
      </article>
  
       <footer>
            Copyright 2020
       </footer>
    </div>
</body>
</html>
<?php
include "../config.php";

$result = mysqli_query($connect,"select mobil.id, mobil.kode, mobil.status_mobil,pemilik.nama from mobil LEFT JOIN pemilik  ON mobil.id_pemilik = pemilik.id;");

$page = "Mobil";
include "../dashboard/header.php";
?>

<div class="isi">

<h3>List Data</h3>
<?php 

if(isset($_GET['status'])){
   echo '<h3> '.$_GET['status'].' </h3>';
}

?>

<a href="tambah-form.php">[+] Tambah Baru</a>

<table border="1" cellpadding="8" cellspacing="0">
        <thead>
            <th>NO</th>
            <th>Kode</th>
            <th>Status</th>
            <th>Telepon</th>
            <th></th>
        </thead>
        <tbody>

        <?php 
            $index = 1;      
        ?>
      
        <?php while ($row = mysqli_fetch_array($result)) { ?>
            <tr>
                <td><?= $index++ ?></td>   
                <td><?= $row['kode'] ?></td>
                <td><?php if($row['status_mobil'] == "0") { echo 'Tersedia';  } else {  echo "Tidak Tersedia"; }?></td>
                <td><?= $row['nama'] ?></td>
                <td>
                    <a href='edit-form.php?id="<?= $row['id'] ?>"'>Edit</a> | 
                    <a href='hapus-proses.php?id=<?= $row['id'] ?>' onClick="return confirm('yakin hapus ?')">Hapus</a> |
                    <a href='detail-form.php?id="<?= $row['id'] ?>"'>Detail</a> 
                </td>
            </tr>
        <?php } ?>  
      
      </tbody>
</table>
</div>
<?php include "../dashboard/footer.php" ?>

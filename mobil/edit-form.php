<?php

include("../config.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: index.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM mobil WHERE id=$id";
$query = mysqli_query($connect, $sql);
$row = mysqli_fetch_assoc($query);

$result = mysqli_query($connect,"select*from pemilik");

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}

$page = "Mobil";
include "../dashboard/header.php";
?>


<div class="isi">
    <h3>Edit Data</h3>
  
    <form action="update-proses.php" method="POST" enctype="multipart/form-data">

    <fieldset>

    <table>
        <input type="hidden" name="id" value=<?= $row['id'] ?> />
              
        <tr>
            <td><label for="kode">Kode: </label></td>
            <td><input type="text" name="kode" id="kode" placeholder="M001" value=<?= $row['kode'] ?> /></td>
        </tr>
        <tr>
            <td><label for="id_pemilik">Pemilik: </label></td>
            <td>
                <select name="id_pemilik" id="id_pemilik">
                <?php while ($data = mysqli_fetch_array($result)) { ?>
                    <option value="<?= $data['id']?>" <?php if($row['id_pemilik'] == $data['id']) { echo "selected"; }?> >
                    <?= $data['kode']. ' - ' . $data['nama'] ?></option>
                <?php } ?>    
                </select>
            </td>
        </tr>
        <tr>
            <td><label for="tahun">Tahun: </label></td>
            <td><input type="text" name="tahun" id="tahun" placeholder="2020" value=<?= $row['tahun'] ?>  ></td>
        </tr>
        <tr>
            <td><label for="warna">Warna: </label></td>
            <td><textarea name="warna" id="warna" ><?= $row['warna'] ?></textarea></td>
        </tr>
        <tr>
            <td><label for="no_plat">No Plat: </label></td>
            <td><input type="text" name="no_plat" id="no_plat" placeholder="no_plat" value=<?= $row['no_plat'] ?> ></td>
        </tr>
        <tr>
            <td><label for="no_mesin">No Mesin: </label></td>
            <td><input type="text" name="no_mesin" id="no_mesin" placeholder="no_mesin" value=<?= $row['no_mesin'] ?>></td>
        </tr>
        <tr>
            <td><label for="no_rangka">No Rangka: </label></td>
            <td><input type="text" name="no_rangka" id="no_rangka" placeholder="no_rangka" value=<?= $row['no_rangka'] ?> ></td>
        </tr>
        <tr>
            <td><label for="status_mobil">Status Mobil: </label></td>
            <td>
                    <select name="status_mobil" id="status_mobil" >
                      <option value="0" <?php if($row['status_mobil'] == "0") { echo "selected"; }?> >Tersedia</option>
                      <option value="1" <?php if($row['status_mobil'] == "1") { echo "selected"; }?> >Tidak Tersedia</option>
                    </select>
            </td>
        </tr>
        <tr>
            <td><label for="merk">Merk: </label></td>
            <td><input type="text" name="merk" id="merk" placeholder="merk" value=<?= $row['merk'] ?>  ></td>
        </tr>
        <tr>
            <td><label for="tipe">Tipe: </label></td>
            <td><input type="text" name="tipe" id="tipe" placeholder="tipe" value=<?= $row['tipe'] ?> ></td>
        </tr>
        <tr>
            <td><label for="foto">Foto: </label></td>
            <td><input type="file" name="foto" id="foto" placeholder="foto" value=<?= $row['foto'] ?> ></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="update" name="update" /></td>
        </tr>
    </table>

</fieldset>

</form>
</div>


<?php include "../dashboard/footer.php" ?>

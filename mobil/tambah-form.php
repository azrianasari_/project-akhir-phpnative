<?php
include "../config.php";

$result = mysqli_query($connect,"select*from pemilik");

$page = "Mobil";
include "../dashboard/header.php";
?>

<div class="isi">
    <h3>Tambah Data</h3>
 
    <form action="tambah-proses.php" method="POST" enctype="multipart/form-data">

        <fieldset>

            <table>
                <tr>
                    <td><label for="kode">Kode: </label></td>
                    <td><input type="text" name="kode" id="kode" placeholder="M001" /></td>
                </tr>
                <tr>
                    <td><label for="id_pemilik">Pemilik: </label></td>
                    <td>
                        <select name="id_pemilik" id="id_pemilik">
                        <?php while ($row = mysqli_fetch_array($result)) { ?>
                            <option value="<?= $row['id']?>"><?= $row['kode']. ' - ' . $row['nama'] ?></option>
                        <?php } ?>  
                            
                        
                        </select>
                       
                    </td>
                </tr>
                <tr>
                    <td><label for="tahun">Tahun: </label></td>
                    <td><input type="text" name="tahun" id="tahun" placeholder="2020" /></td>
                </tr>
                <tr>
                    <td><label for="warna">Warna: </label></td>
                    <td><textarea name="warna" id="warna"></textarea></td>
                </tr>
                <tr>
                    <td><label for="no_plat">No Plat: </label></td>
                    <td><input type="text" name="no_plat" id="no_plat" placeholder="no_plat" /></td>
                </tr>
                <tr>
                    <td><label for="no_mesin">No Mesin: </label></td>
                    <td><input type="text" name="no_mesin" id="no_mesin" placeholder="no_mesin" /></td>
                </tr>
                <tr>
                    <td><label for="no_rangka">No Rangka: </label></td>
                    <td><input type="text" name="no_rangka" id="no_rangka" placeholder="no_rangka" /></td>
                </tr>
                <tr>
                    <td><label for="status_mobil">Status Mobil: </label></td>
                    <td>
                            <select name="status_mobil" id="status_mobil" >
                              <option value="0">Tersedia</option>
                              <option value="1">Tidak Tersedia</option>
                            </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="merk">Merk: </label></td>
                    <td><input type="text" name="merk" id="merk" placeholder="merk" /></td>
                </tr>
                <tr>
                    <td><label for="tipe">Tipe: </label></td>
                    <td><input type="text" name="tipe" id="tipe" placeholder="tipe" /></td>
                </tr>
                <tr>
                    <td><label for="foto">Foto: </label></td>
                    <td><input type="file" name="foto" id="foto" placeholder="foto" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="tambah" name="tambah" /></td>
                </tr>
            </table>

        </fieldset>

    </form>
</div>


<?php include "../dashboard/footer.php" ?>

<?php

include("../config.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: index.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM mobil WHERE id=$id";
$query = mysqli_query($connect, $sql);
$row = mysqli_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}

$page = "Mobil";
include "../dashboard/header.php";
?>

<div class="isi">
    <h3>Detail Data</h3>
    <form action="update-proses.php" method="POST">
        <fieldset>
            <table>
                <input type="hidden" name="id" value=<?= $row['id'] ?> />
                <tr>
                    <td><label for="kode">Kode: <?= $row['kode']?>  </label></td>
                </tr>
                <tr>
                    <td><label for="tahun">Tahun: <?= $row['tahun']?>  </label></td>
                </tr>
                <tr>
                    <td><label for="warna">Warna: <?=$row['warna']?> </label></td>
                </tr>
                <tr>
                    <td><label for="no_plat">No plat: <?=$row['no_plat']?> </label></td>
                </tr> 
                <tr>
                    <td><label for="no_mesin">No mesin: <?=$row['no_mesin']?> </label></td>
                </tr> <tr>
                    <td><label for="no_rangka">No rangka: <?=$row['no_rangka']?> </label></td>
                </tr>
                <tr>
                    <td><label for="status_mobil">Status mobil:  
                    <?php if($row['status_mobil'] == "0") { echo 'Tersedia'; } else { echo "Tidak Tersedia";  }?> 
                    </label></td>
                </tr>
                <tr>
                    <td><label for="merk">Merk: <?= $row['merk']?> </label></td>
                </tr>
                <tr>
                    <td><label for="tipe">Tipe:  <?= $row['tipe']?> </label></td>
                </tr>
                <tr>
                    <td><label for="foto">Foto:</label>
                    <br>
                    <img src="upload/<?= $row['foto']?>" alt="" width="30%"> 
                    <td>
                   
                </tr>
                <tr>
                    <td><a href="index.php">Kembali</td>
                </tr>
            </table>

        </fieldset>
    </form>
</div>


<?php include "../dashboard/footer.php" ?>